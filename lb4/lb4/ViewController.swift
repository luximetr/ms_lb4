//
//  ViewController.swift
//  lb4
//
//  Created by Александр on 09.03.17.
//  Copyright © 2017 Alexandr Orlov. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    //MARK: IBOutlets
    @IBOutlet weak var textField: UITextField!
    @IBOutlet var textView: UITextView!

    @IBOutlet weak var sourceTextField: UITextField!
    //MARK: fields
    var strings: NSMutableArray = []
    
    //MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.textField.text = UserDefaults.standard.value(forKey: "text") as! String?
        if let stringArray  = NSArray(contentsOf: self.plistURL()) {
            self.strings = NSMutableArray(array: stringArray)
        }
        self.textView.text = self.strings.componentsJoined(by: "\n")
    }
    
    //MARK: IBActions
    @IBAction func actionTapOnSaveButton(_ sender: Any) {
        UserDefaults.standard.set(self.textField.text!, forKey: "text")
        UserDefaults.standard.synchronize()
    }
    
    @IBAction func actionTapOnSaveTextViewButton(_ sender: Any) {
        self.strings.add(self.sourceTextField.text!)
        self.textView.text = self.strings.componentsJoined(by: "\n")
        (self.strings as NSArray).write(to: self.plistURL(), atomically: true)
    }
    
    //MARK: Private
    func plistURL() -> URL {
        return (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last?.appendingPathComponent("data.plist"))!
    }
}

