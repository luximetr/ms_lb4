//
//  Student.swift
//  lb4_2
//
//  Created by Александр on 09.03.17.
//  Copyright © 2017 Alexandr Orlov. All rights reserved.
//

import Foundation

class Student: NSObject, NSCoding {
    var name: String = ""
    var surname: String = ""
    
    init(name: String, surname: String) {
        self.name = name
        self.surname = surname
    }
    
    override public var description: String { return "Name: \(name), surname: \(surname)" }
    
    public required init?(coder aDecoder: NSCoder) {
        self.name = aDecoder.decodeObject(forKey: "name") as! String
        self.surname = aDecoder.decodeObject(forKey: "surname") as! String
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(self.name, forKey: "name")
        aCoder.encode(self.surname, forKey: "surname")
    }
}
