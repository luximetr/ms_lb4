//
//  ViewController.swift
//  lb4_2
//
//  Created by Александр on 09.03.17.
//  Copyright © 2017 Alexandr Orlov. All rights reserved.
//

import UIKit
import FMDB

class ViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var surnameTextField: UITextField!
    @IBOutlet weak var textView: UITextView!
    
    //MARK: Fields
    var students: Array<Student> = []
    var db: FMDatabase! = nil
    
    //MARK: File Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let documentsDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first! as String
        let pathToDatabase = documentsDirectory.appending("database.sqlite")
        self.db = FMDatabase(path: pathToDatabase)
        self.db.open()
        do {
            try self.db.executeUpdate("CREATE TABLE IF NOT EXISTS students (name TEXT, surname TEXT)", values: nil)
        } catch {}
        
        do {
            let plistData = try Data(contentsOf: self.plistURL())
            self.students = NSKeyedUnarchiver.unarchiveObject(with: plistData) as! Array<Student>
            self.textView.text = self.studentsAsString(with: self.students)
        } catch {}
    }

    //MARK: IBActions
    @IBAction func actionTapOnSave(_ sender: Any) {
        if (self.nameTextField.text?.isEmpty)! || (self.surnameTextField.text?.isEmpty)! {
            return
        }
        
        self.students.append(Student(name: self.nameTextField.text!, surname: self.surnameTextField.text!))
        self.textView.text = self.studentsAsString(with: self.students)
        let data = NSMutableData()
        let archiver = NSKeyedArchiver(forWritingWith: data)
        archiver.outputFormat = .xml
        archiver.encode(self.students, forKey: "root")
        archiver.finishEncoding()
        data.write(to: self.plistURL(), atomically: true)
        
        let sqlRequest = "INSERT INTO students (name, surname) VALUES ('\(self.nameTextField.text!)', '\(self.surnameTextField.text!)')"
        do {
            try self.db.executeUpdate(sqlRequest, values: nil)
        } catch {
        }
    }
    
    @IBAction func actionTapOnSearchButton(_ sender: Any) {
        let sqlRequest = "SELECT * FROM students WHERE name LIKE '\(self.nameTextField.text!)'"
        do {
            let result = try db.executeQuery(sqlRequest, values: nil)
            var string = ""
            while result.next() {
                string.append("\(result.string(forColumnIndex: 0)!) \(result.string(forColumnIndex: 1)!)\n")
            }
            self.textView.text = string
        } catch {
        }
    }
    
    //MARK: Private
    func plistURL() -> URL {
        return (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last?.appendingPathComponent("data1.plist"))!
    }
    
    func studentsAsString(with array:Array<Student>) -> String {
        var resultString = ""
        for i in 0..<students.count {
            resultString.append(String(describing: self.students[i]))
            resultString.append("\n")
        }
        return resultString
    }
}

